Component({
  data: {
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#3cc51f",
    list: [{
      pagePath: "/pages/index/index",
      iconPath: "/pages/image/icon_component.png",
      selectedIconPath: "/pages/image/icon_component_HL.png",
      text: "菜单1"
    }, {
      pagePath: "/pages/index2/index2",
      iconPath: "https://file.snailpet.cn/wxApplet/home_1.png", //支持网络图片
      selectedIconPath: "https://file.snailpet.cn/wxApplet/home_3.png",//支持网络图片
      text: "菜单2"
    },{
      pagePath: "/pages/index3/index3",
      iconPath: "https://file.snailpet.cn/wxApplet/vipcd.png", //支持网络图片
      selectedIconPath: "https://file.snailpet.cn/wxApplet/home_1.png",//支持网络图片
      text: "菜单3"
    },{
      pagePath: "/pages/setting/setting",
      iconPath: "https://file.snailpet.cn/wxApplet/home_4.png", //支持网络图片
      selectedIconPath: "https://file.snailpet.cn/wxApplet/home_1.png",//支持网络图片
      text: "工具"
    },{
        pagePath: "/pages/about/about",
        iconPath: "/pages/image/appointment_normal.png",
        selectedIconPath: "/pages/image/appointment_selected.png",
        text: "关于3"
    }]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
    }
  }
})