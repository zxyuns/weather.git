# weather

#### Description
微信小程序，查看天气，另外加入个人计划的展示，换肤，慢慢把一些好的功能模块迁移到里面。
该项目的起源来源于下面两个项目：
1：[https://github.com/myvin/quietweather.git](https://github.com/myvin/quietweather.git)
2：[https://github.com/weijhfly/mytarget](https://github.com/weijhfly/mytarget)

文章地址：
1：[https://juejin.im/post/5b39bbcc5188252ce018c745](https://juejin.im/post/5b39bbcc5188252ce018c745)
2: [https://juejin.im/post/5c271e086fb9a04a0a5f44ad](https://juejin.im/post/5c271e086fb9a04a0a5f44ad)

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
